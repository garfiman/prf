DROP TABLE IF EXISTS goods;
CREATE TABLE goods(id serial PRIMARY KEY, name VARCHAR(255), price INTEGER);
DROP SEQUENCE IF EXISTS hibernate_sequence;
CREATE SEQUENCE hibernate_sequence START 1;

DROP TABLE IF EXISTS transacts;
CREATE TABLE transacts(id serial PRIMARY KEY, date VARCHAR(255), goodieid INTEGER, sum INTEGER);
DROP SEQUENCE IF EXISTS hibernate_sequence_transactions;
CREATE SEQUENCE hibernate_sequence_transactions START 1;