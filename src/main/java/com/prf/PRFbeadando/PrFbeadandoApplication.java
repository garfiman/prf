package com.prf.PRFbeadando;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrFbeadandoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrFbeadandoApplication.class, args);
	}

}
