package com.prf.PRFbeadando.controllers;

import java.util.List;

import com.prf.PRFbeadando.models.Goodie;
import com.prf.PRFbeadando.models.GoodieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@CrossOrigin(origins = "*")
public class GoodieController {

    GoodieService goodieService;

    @Autowired
    public GoodieController(GoodieService goodieService) {
        this.goodieService = goodieService;
    }

    @PostMapping(path="/goodie", consumes = "application/json")
    public String newGoodie(@RequestBody Goodie goodie){
        try {
            this.goodieService.addGoodie(goodie);
            return "Success";
        } catch (Exception e) {
            System.out.println(e);
            return "Error";
        }
    }

    @GetMapping("/goodies")
    public List<Goodie> getAllGoodies(){
        try {
            return this.goodieService.getAllGoodies();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @GetMapping("/goodie")
    public Goodie getGoodieById(@RequestParam int id){
        try {
            return this.goodieService.getGoodieById(id);
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }

    @DeleteMapping("/goodie")
    public String deleteGoodieById(@RequestParam int id){
        try {
            this.goodieService.deleteGoodieById(id);
            return "Delete Successful";
        } catch (Exception e) {
            System.out.println(e);
            return "Error during delete";
        }
    }
    
    
}
