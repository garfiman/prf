package com.prf.PRFbeadando.models;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodieRepository extends JpaRepository<Goodie, Integer> {

    
}
