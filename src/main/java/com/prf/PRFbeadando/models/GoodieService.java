package com.prf.PRFbeadando.models;

import java.util.List;

public interface GoodieService {

    void addGoodie(Goodie goodie);

    List<Goodie> getAllGoodies();

    Goodie getGoodieById(int id);

    void deleteGoodieById(int id);    
}
