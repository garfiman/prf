package com.prf.PRFbeadando.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "transacts")
public class Transaction {

    @Id
    @GeneratedValue
    private int id;

    private String date;

    private int goodieid;

    private int sum;

    
    public Transaction() {
    }

    public Transaction(int id ,String date, int goodieid, int sum) {
        this.id = id;
        this.date = date;
        this.goodieid = goodieid;
        this.sum = sum;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getGoodieId() {
        return goodieid;
    }

    public void setGoodieId(int goodieid) {
        this.goodieid = goodieid;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Transaction [date=" + date + ", goodieId=" + goodieid + ", id=" + id + ", sum=" + sum + "]";
    }
   
    
}
