package com.prf.PRFbeadando.models;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodieServiceImpl implements GoodieService {

    GoodieRepository goodieRepository;

    
    @Autowired
    public GoodieServiceImpl(GoodieRepository goodieRepository) {
        this.goodieRepository = goodieRepository;
    }

    @Override
    public void addGoodie(Goodie goodie) {
        this.goodieRepository.save(goodie);
        
    }

    @Override
    public List<Goodie> getAllGoodies() {
        List<Goodie> list = this.goodieRepository.findAll();
        return list;
    }

    @Override
    public Goodie getGoodieById(int id) {
        Goodie goodie = this.goodieRepository.findById(id).get();
        return goodie;
    }

    @Override
    public void deleteGoodieById(int id) {
        this.goodieRepository.deleteById(id);
    }
    
}
